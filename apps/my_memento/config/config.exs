use Mix.Config
config :mnesia,
       dir: '.mnesia/#{Mix.env}/#{node()}'        # Notice the single quotes