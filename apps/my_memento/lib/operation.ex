defmodule Operation do

  #  Create tables of db
  def create_tables do
    Memento.Table.create!(Models.User)
    Memento.Table.create!(Models.GServer)
    Memento.Table.create!(Models.Connection)
  end

  #  Some user for example
  def map_user do
    users = [
      %Models.User{id_u: 1, username: "ftadev", password: "123", mqtt_flag: true},
      %Models.User{id_u: 2, username: "mikdev", password: "123", mqtt_flag: true},
      %Models.User{id_u: 3, username: "hrbdev", password: "123", mqtt_flag: true},
      %Models.User{id_u: 4, username: "mmrdev", password: "123", mqtt_flag: true},
      %Models.User{id_u: 5, username: "ahsdev", password: "123", mqtt_flag: true}
    ]
  end

  #  Insert one user
  def insert(user, 1) do
    Memento.transaction! fn ->
      Memento.Query.write(user)
    end
  end

  #  Insert list of users
  def insert(user_list, 2) do
    Enum.each(user_list, fn (user) -> insert(user, 1) end)
  end

  #  Get all users
  def get_all do
    Memento.transaction fn ->
      Memento.Query.all(Models.User)
    end
  end

  #  Get specific user
  def get_item(id) do
    Memento.transaction fn ->
      Memento.Query.read(Models.User, id)
    end
  end

  #  Select specific user
  def select(guards) do
    Memento.transaction! fn ->
      Memento.Query.select(Models.User, guards)
    end
  end

  #  Query for select
  def set_guard(username) do
    guard = [
      {:==, :username, username},
    ]
    guard
  end

  #  Another example for guard:
  #  guards =
  #  {:and
  #     {:>=, :year, 2010},
  #     {:or,
  #       {:==, :director, "Quentin Tarantino"},
  #       {:==, :director, "Steven Spielberg"},
  #     }
  #   }

  #  Insert sample users
  def sample 1 do
    map_user
    |> insert 2
  end

  #  Get sample user
  def sample(username) do
    set_guard(username)
    |> select
  end

  # Delete a record by primary key
  def del_item(id_u) do
    Memento.transaction! fn ->
      Memento.Query.delete(Models.User, id_u)
    end
  end

  #  Delete a record by passing the full object
  def del_record(record) do
    Memento.transaction! fn ->
      Memento.Query.delete_record(record)
    end
  end

  # Write into disk
  def persist_to_disk do
    # List of nodes where you want to persist
    nodes = [node()]

    # Create the schema
    Memento.stop
    Memento.Schema.create(nodes)
    Memento.start

    # Create your tables with disc_copies (only the ones you want persisted on disk)
    Memento.Table.create!(Models.User, disc_copies: nodes)
    Memento.Table.create!(Models.GServer, disc_copies: nodes)
    Memento.Table.create!(Models.Connection, disc_copies: nodes)
  end


end
