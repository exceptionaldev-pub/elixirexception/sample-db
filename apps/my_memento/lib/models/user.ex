defmodule Models.User do
  use Memento.Table,
      attributes: [
        :id_u,
        :username,
        :password,
        :mqtt_flag
      ],
#      index: [:status, :author],
      type: :ordered_set,
      autoincrement: true

end

# A Memento Table should be specify its attributes, type and other options.
# At least two attributes are required, where the first one is the primary-key of the table.
