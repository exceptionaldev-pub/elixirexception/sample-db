defmodule MyEcto.MixProject do
  use Mix.Project

  def project do
    [
      app: :my_ecto,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {MyEcto.Application, []}
    ]
  end

  defp deps do
    [
      {:mariaex, ">= 0.8.4"},
      {:ecto, "~> 2.2"},
      {:plsm, "~> 2.2.0"}
    ]
  end
end
