defmodule MyEcto.Application.Genserver do
  use Ecto.Schema

@primary_key {:id_g, :integer, []}
  @foreign_key_type :id_u
  schema "genserver" do
#    field :id_g, :integer
    field :id_u, :integer
    field :gpid, :string

  end
  def changeset(struct, params \\ %{}) do
    struct
    |> Ecto.Changeset.cast(params, :id_g, :id_u, :gpid)
  end
end
