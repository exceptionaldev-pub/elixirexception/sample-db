defmodule MyEcto.Application.User do
  use Ecto.Schema

@primary_key {:id_user, :integer, []}
  schema "user" do
#    field :id_user, :integer
    field :username, :string
    field :password, :string
    field :mqtt_flag, :boolean

  end
  def changeset(struct, params \\ %{}) do
    struct
    |> Ecto.Changeset.cast(params, :id_user, :username, :password, :mqtt_flag)
  end
end
