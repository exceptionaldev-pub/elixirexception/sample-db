defmodule MyEcto.Application.Connection do
  use Ecto.Schema

@primary_key {:id_conn, :integer, []}
  @foreign_key_type :id_g
  schema "connection" do
#    field :id_conn, :integer
    field :id_g, :integer
    field :spid, :string

  end
  def changeset(struct, params \\ %{}) do
    struct
    |> Ecto.Changeset.cast(params, :id_conn, :id_g, :spid)
  end
end
