defmodule MyEcto.Application do

  use Application

  def start(_type, _args) do
    children = [
      # Just add "MyEcto.Repo," to children
      MyEcto.Repo,
    ]


    opts = [strategy: :one_for_one, name: MyEcto.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
