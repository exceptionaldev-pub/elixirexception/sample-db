defmodule MyEcto do
  import Ecto.Query, only: [from: 2]

  def select_all do
    MyEcto.Application.User
    |> MyEcto.Repo.all
  end

  def select_with_query do
#    query = MyEcto.Application.User
#            |> where(mqtt_flag: true)
#            |> select([u], username)
#
#    MyEcto.Repo.all(query)
  end

  def select2_with_query(username, password) do
    query = from u in MyEcto.Application.User,
                 where: u.username == ^username
                 and u.password == ^password,
                 select: u

    MyEcto.Repo.all(query)
  end

  def join_with_query do
#    query = MyEcto.Application.User
#            |> join(:inner, [u], g in MyEcto.Application.Genserver, u.id_user == g.id_u)
#            |> select([u, g], {u.id_user, u.username, g.id_u, g.id_u})
#
#    #        Ecto.Query.exclude(query, :distinct)
#    MyEcto.Repo.all(query)
  end

  def join2_with_query(username, password) do
    query = from user in MyEcto.Application.User,
                 join: genserver in MyEcto.Application.Genserver,
                 where: user.id_user == genserver.id_u
                        and user.username == ^username
                 and user.password == ^password,
                 select: {user, genserver}
    MyEcto.Repo.all(query) # Return a list of tuples: {user, genserver}
  end

  def insert(user) do
    if user.mqtt_flag == true do
      pid = spawn fn -> 1 + 2 end
      genserver = %MyEcto.Application.Genserver{
        id_u: user.id_user,
        gpid: Kernel.inspect(pid)
      }
      MyEcto.Repo.insert(genserver)
    end
  end

  def sample do
    users = select_all
    Enum.each(users, fn (u) -> insert(u)  end)
  end



end
