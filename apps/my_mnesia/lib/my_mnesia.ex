defmodule MyMnesia do
  alias :mnesia, as: Mnesia

  def create_db do
    Mnesia.create_schema([node()])
    Mnesia.start()
  end

  def create_table do
    Mnesia.create_table(Person, [attributes: [:id, :name, :job]])
  end

  def transaction do
    data_to_write = fn ->
      Mnesia.write({Person, 4, "Marge Simpson", "home maker"})
      Mnesia.write({Person, 5, "Hans Moleman", "unknown"})
      Mnesia.write({Person, 6, "Monty Burns", "Businessman"})
      Mnesia.write({Person, 7, "Waylon Smithers", "Executive assistant"})
    end
    Mnesia.transaction(data_to_write)
  end

  def read_data do
    data_to_read = fn ->
      Mnesia.read({Person, 6})
    end
    Mnesia.transaction(data_to_read)
  end

  def sample do
    case Mnesia.create_table(Person, [attributes: [:id, :name, :job, :age]]) do
      {:atomic, :ok} ->
        Mnesia.add_table_index(Person, :job)
        Mnesia.add_table_index(Person, :age)
      {:aborted, {:already_exists, Person}} ->
        case Mnesia.table_info(Person, :attributes) do
          [:id, :name, :job] ->
            Mnesia.transform_table(
              Person,
              fn ({Person, id, name, job}) ->
                {Person, id, name, job, 21}
              end,
              [:id, :name, :job, :age]
            )
            Mnesia.add_table_index(Person, :age)
          [:id, :name, :job, :age] ->
            :ok
          other ->
            {:error, other}
        end
    end
  end

end