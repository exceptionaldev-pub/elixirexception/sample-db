defmodule MyMongo do
#  Other functions are described here: https://hexdocs.pm/mongodb/Mongo.html#summary

  def connection do
    {:ok, conn} = Mongo.start_link(
      url: "mongodb://localhost:27017/my_mongo",
#      username: "user",
#      password: "pass",
      auth_source: "admin"
    )
    conn
  end

  def insert(conn) do
    Mongo.insert_one!(conn, "user", %{city: "tehran"})
  end

  def get do
    Mongo.find(:mongo, "users", %{"$and" => [%{email: "my@email.com"}, %{first_name: "first_name"}]})
    Mongo.find(:mongo, "users", %{"$or" => [%{email: "my@email.com"}, %{first_name: "first_name"}]})
    Mongo.find(:mongo, "users", %{email: %{"$in" => ["my@email.com", "other@email.com"]}})
  end

  def sample do
    connection
    |> update
  end

  def update(conn) do
    Mongo.update_many(
      conn,
      "user",
      %{city: "tehran"},
      %{
        "$set": %{
          city: "teh"
        }
      }
    )
  end

end

