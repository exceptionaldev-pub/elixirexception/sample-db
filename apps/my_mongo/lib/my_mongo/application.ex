defmodule MyMongo.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      worker(Mongo, [[name: :mongo, database: "my_mongo" , pool: DBConnection.Poolboy]])
    ]

    opts = [strategy: :one_for_one, name: MyMongo.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
