defmodule MyMongo.MixProject do
  use Mix.Project

  def project do
    [
      app: :my_mongo,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      applications: [:mongodb, :poolboy, :logger],
      #      extra_applications: [:logger],
      mod: {MyMongo.Application, []}
    ]
  end

  defp deps do
    [
      {:mongodb, ">= 0.0.0"},
      {:poolboy, ">= 0.0.0"}
    ]
  end
end
