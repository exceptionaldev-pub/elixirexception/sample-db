defmodule MyAmnesia.MixProject do
  use Mix.Project

  def project do
    [
      app: :my_amnesia,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [ # This dependency has error, so you can copy amnesia dep manually
      # and then run mix deps.compile
      #      {:amnesia, "~> 0.2"},
    ]
  end
end
